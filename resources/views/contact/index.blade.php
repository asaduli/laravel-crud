<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel Curd using Ajax</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>

<body>

    <div class="container">

        <div class="row my-4">

            <h1> Laravel Curd Operation using Ajax</h1>

            <!-- Outpur Area -->
            <div class="col-8">
                <table class="table table-striped table-hover table-info my-5">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id='tbody'>

                    </tbody>
                </table>


            </div>
            <!-- Input Area -->
            <div class="col-4">
                <div class="card text-center" style="width:400px;">
                    <div class="card-body">
                        <h5 class="card-title">Add New Contact</h5>

                        <div class="mb-3">
                            <label for="name" class="form-label">Full Name</label>
                            <input type="text" class="form-control" id="name">

                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" id="phone">

                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="email">

                        </div>


                        <button class="btn btn-primary" id="save">Save</button>

                    </div>

                </div>

            </div>
            <div>
                <h5>Output</h5>
                @if(isset($data)){
                <p>{{$data}}</p>
                }
                @endif
            
            </div>


        </div>


        <script>
            const tbody = document.querySelector('#tbody');
            const URL = 'contact/all';

            window.onload = function() {
                fetchData()
                const save = document.querySelector('#save');
                save.addEventListener('click', () => {
                    storeData();
                });


            }

            function fetchData() {
                axios.get(URL)
                    .then((res) => {

                        res.data.forEach(user => {

                            makeElement(tbody, user)

                        });

                    })
                    .catch((error) => console.log(error));

            }

            function makeElement(parrentElement, User) {

                let tr = document.createElement('tr');
                let tdata1 = document.createElement('td');
                let tdata2 = document.createElement('td');
                let tdata3 = document.createElement('td');
                let tdata4 = document.createElement('td');
                let tdata5 = document.createElement('td');
                tdata1.innerHTML = User.id;
                tdata2.innerHTML = User.name;
                tdata3.innerHTML = User.phone;
                tdata4.innerHTML = User.email;
                tdata5.innerHTML = `<button class="btn btn-info mx-1 btn-outline-dark"> Edit</button>` + `<button class="btn btn-danger btn-outline-dark mx-1"> Delete</button>`;
                tr.appendChild(tdata1)
                tr.appendChild(tdata2)
                tr.appendChild(tdata3)
                tr.appendChild(tdata4)
                tr.appendChild(tdata5)
                parrentElement.appendChild(tr);
            }



            function storeData() {

                let url = 'contact/store'
                let inputname = document.querySelector('#name');
                let inputphone = document.querySelector('#phone');
                let inputemail = document.querySelector('#email');
                let contact = {
                    name: inputname.value,
                    phone: inputphone.value,
                    email: inputemail.value,
                }

                axios.post(url, contact)
                .then(res=>{
                  
                    let data=res.data;
                    makeElement(tbody,res.data);
               

                 
                   inputname.value=''
                   inputphone.value=''
                   inputemail.value=''

                })
                .catch(err=>console.log(err));
            


            }
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>